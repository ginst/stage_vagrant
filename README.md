created & maintained by [**Geodetic institute of Slovenija**](<http://www.gis.si/>)
#STAGE
---
Stage (STAtistics & GEography) is an application for displaying and disseminating geospatial statistical data. It is an efficient web tool for presenting and providing statistical values regarding space. The application enables importing of statistical and geographical file formats, which are the basis for creating an attractive and informative web display of statistical phenomena in space.  
A running instance of application is located at http://gis.stat.si/stage2

##Contents
---
* The Motivation
* Getting started
	* Prerequisites
	* Running the VM
	* Post Installation
	* Deploying
	* Troubleshooting
* Additional Resources
* Contribute
	* Reporting issues
* License

### The Motivation
---
**Our motivation is that we need to visualize space related data in order to understand it.**

> 99 percent of all statistics only tell 49 percent of the story.

--Ron DeLegge II, Gents with No Cents

> The application of GIS is limited only by the imagination of those who use it.

--Jack Dangermond, Esri

## Getting started
---
This is an attempt to provide an easy and replicatable way to rapidly stand up a STAGE instance for development or other purposes in an automated way, using Vagrant.
### Prerequisites:
* Vagrant; Installation of vagrant https://www.vagrantup.com/ - vagrant provides the mechanism for configuring and provisioning the machine.  Vagrant can support VirtualBox or cloud hosted platforms.  This vagrant instance is initially focusing on virtualbox.

* VirtualBox; The geoserver instance will run within virtualbox https://www.virtualbox.org/wiki/Downloads - vagrant will create a virtualbox vm and provision it

### Cloning master
To get started, first clone the repository. If you're not familiar with Git, visit the Git homepage to download Git for your platform.

First, clone the repository:

```
$ git clone https://bitbucket.org/sebastjan_meza/stage_vagrant.git
$ cd stage_vagrant
```


### Running the VM
With vagrant and virtualbox installed on the host machine, the Stage machine can be instantiated by cloning or copying this repo into a directory, and from a command line, going to the directory containing the vagrantfile and provision.sh scripts, and simply running 'vagrant up'

Vagrant will create the vm, and then will fetch Ubuntu 16.04 (xenial64) and then run the provisioner script.  The provisioner script, 'provision.sh' is just a BASH shell script that will run to install dependencies, and then install Apache, PostGIS, GeoServer, Drupal, ....

### Post-Installation
After the VM is up, the user needs to ...

* set up default variable parameters in the settings tab of the STAGE admin
* create spatial units
* create tree of variables including root (make sure to translate all variable names including root)
* import SHP geometry
* import variable values
* publish variables

To view the web interface go to

* STAGE admin go to http://guest_ip ; e.g. http://192.168.66.90/stage/admin
* STAGE client go to http://guest_ip/stage/client
* GeoServer client go to http://guest_ip:8080/geoserver
* Postgres DB client go to http://guest_ip/phppgadmin/

### Deploying
* Command call demos:

```
// connect to vagrant
$ vagrant ssh

// List of available commands in virtual machine:
$ tools <TAB> to autocomplete

// reinstal stage related modules
$ tools project/reinstall_stage DEVELOPMENT

// manually install geoserver
$ tools os/install_geoserver DEVELOPMENT

```
### Troubleshooting
* Geoserver: Check if geoserver is running. Script os/install_geoserver depends on a remote repository if it fails to execute manually install geoserver and reimport required styles ... os/config_geoserver DEVELOPMENT ...
* If an install script fails to excecute, check if the script file has UNIX line endings (EOL).

## Additional Resources
---
* A detailed user manual can be found at: http://gis.stat.si/stage2rc/admin/sites/default/files/STAGE_admin_client_manuals_v2.01.pdf

## Contribute
---
### Reporting issues
* To file a bug report, please visit the BitBucket issues page. It's great if you can attach code (test cases and fixes for bugs, and test cases and a proposed implementation for features), but reproducible bug reports are also welcome.

## License
---
* Free Software, EUPL license - [European Union Public License](<https://joinup.ec.europa.eu/collection/eupl/introduction-eupl-licence>)
