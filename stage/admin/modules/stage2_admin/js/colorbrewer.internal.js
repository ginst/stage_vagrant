(function ($){
  Drupal.behaviors.gimOlMapContentElement = {
    attach: function (context, settings){
    // read default_value
    var selectedName = $("#colorBrewerInput").val();

    /*
    * Function creates color sheme from color array
    *
    * @colors           array     Array with colors
    */
    function colorSquares(colors, name){

      var colorDiv = $("<div class='colorDiv' data-name='"+name+"' style=' width: 100px;height: 25px;cursor:pointer'></div>");

      $.each(colors, function(index,value){
        colorDiv.append($("<div style='width: 15px;height: 15px;margin: 0px;outline: 1px solid;display: inline-block;float: left;background: "+value+";' />"));
      })

      if(selectedName == name){
        colorDiv.append($("<div class='paletteSelection paletteBrewerDisplay' />"));
        }else{
        colorDiv.append($("<div class='paletteSelection' />"));
      }

      return colorDiv;
    }

    // empty container first
    $("#colorBrewerContainer").empty();
    // create palette for every colorbrewer item
    // colorbrewer defined in colorbrewer.min.js
    $.each(colorbrewer, function(index,value){
      if(value[5]){
      $("#colorBrewerContainer").append(colorSquares(value[5], index));
      }
    });

    var container = $('#colorBrewerContainer');
    var scrollTo = $("[data-name='"+selectedName+"']");

    // click listener - when colorbrewer div is visible, set slider to position of default element
    var onceFlag = true;
    $(document).click(function(){
      function process(){
        if(container.is(":visible") && onceFlag){
        container.scrollTop(scrollTo.offset().top - container.offset().top + container.scrollTop() - 100);
        onceFlag = false;
        }
      }
      setTimeout(process,300);
    });

    // clicked color scheme save to drupal fapi element
    $(".colorDiv").click(function(){
      $(".paletteBrewerDisplay").removeClass("paletteBrewerDisplay");
      $(this).children(".paletteSelection").addClass("paletteBrewerDisplay");
      $("#colorBrewerInput").val($(this).data("name")).trigger('change');
    });
  }
}
})(jQuery);
