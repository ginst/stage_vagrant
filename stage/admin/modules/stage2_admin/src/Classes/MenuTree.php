<?php

namespace Drupal\stage2_admin\Classes;

class MenuTree{


	/**
	* Function loads menu tree structure from var_tree table
	*
	* Used in:
	* src/Form/StageMenuTreeForm
	* src/Form/StageMenuTreeAddForm
	*
	* @param array $condition [var_tree ids as condition]
	* @param boolean $fetchAllAssoc [fetch associative array instead of stdClass]
	*
	*/
	public static function GetMenuTree($condition = [], $fetchAllAssoc = false){

		$query =  db_select('s2.var_tree', 'tree');
		$query -> join('s2.var_names', 'b', 'tree.id = b.var_tree_id');
		$query -> fields('tree')
			   -> fields('b', ['name'])
			   -> addField('b', 'id', 'name_id');
		$query -> addExpression('position::int', 'hej');
		$query -> orderBy('hej', 'ASC');

		$or = db_or();
		foreach($condition as $con){
			$or->condition("tree.id", $con);
		}
		if(!empty($condition)){
			$query->condition($or);
		}

		if(!$fetchAllAssoc){
			return $query->execute()->fetchAll();
		}else{
			return $query->execute()->fetchAllAssoc('id');
		}

	}

}
