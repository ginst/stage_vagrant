<?php
namespace Drupal\stage2_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\stage2_admin\StageDatabase;
use Drupal\stage2_admin\StageDatabaseSM;
use Drupal\stage2_admin\StageFormsCommon;

class StageVariablesForm extends FormBase{

	/**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID(){
    return 'stage_variables_form';
	}

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Butons that are deisplayed above the tableselect
		$form['import'] = array(
			'#type' => 'link',
			'#title' => t('Add variable (Shape & CSV)'),
			'#attributes' => array(
			'class' => array('button'),
			),
			'#url' => Url::fromRoute('stage2_admin.batchImport'),
		);

		$form['import_px'] = array(
			'#type' => 'link',
			'#title' =>  t('Add variable (PX)'),
			'#attributes' => array(
				'class' => array('button'),
			),
			'#url' => Url::fromRoute('stage2_admin.batchImportPX'),
		);

    $form['update_px'] = array(
			'#type' => 'link',
			'#title' =>  t('Update variables (PX)'),
			'#attributes' => array(
				'class' => array('button'),
			),
			'#url' => Url::fromRoute('stage2_admin.variablesUpdatePX'),
		);

		$form['delete'] = array(
			'#type' => 'submit',
			'#value' => t('Delete selected'),
		);
		$form['publish'] = array(
			'#type' => 'submit',
			'#value' => t('Publish selected on date'),
		);

		$form['unpublish'] = array(
			'#type' => 'submit',
			'#value' => t('Unpublish selected'),
		);

		// read spatial_layers
		$spatial_layers = StageDatabase::loadSpatialLayers();
		$variables=array();
		
    // spatial layer selector
    if(!empty($spatial_layers)){
  
      $default = !empty($spatial_layers_selector) ? $spatial_layers_selector : 0;
  
      $sls_options[0] = "- ".t("All")." -";
      foreach($spatial_layers as $key => $value){
      $sls_options[$key] = $value['name'];
      }
  
       $form['spatial_layers_selector'] = array(
      '#title' => t('Select spatial layer'),
      '#type' => 'select',
      '#options' => $sls_options,
      '#default_value' => $default,
      '#ajax' => array(
        'callback' => array($this, 'ajax_spatial_layer_callback'),
        'wrapper' => 'variables_table',
        'method' => 'replace',
        'effect' => 'fade',
      ),
       );
  
     // read variables from database
     //$variables = StageDatabaseSM::loadVariables();
     $queryArray = ($default == 0)?array():array("spatial_layer.id" => $default);
     $variables = StageDatabase::loadVariables2($queryArray);
    }

		// downloads counter
		$downloads = StageDatabase::countDownloads();
		$downar = array();
		foreach($downloads as $row){
			$downar[$row->var_names_id][$row->id] = $row->count;
		}

		// Tableselect header column names
		$header = array(
			'name' => t('Name'),
			'short_name' => t('Acronym'),
			'layer' => t('Spatial layer'),
			'published' => t('Published'),
			'counter' => t('Download counter'),
		);

		$tree_menu=StageFormsCommon::treeStructure();

		// Options to be displayed in the tableselect
		$options = array();
		
   StageFormsCommon::getTableOptions($options,$variables,$tree_menu,$downar);
    
	  $form['table'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $options,
			'#id' => 'tableselect_id',
			'#js_select' => false,
			'#empty' => t('No variable has not yet been added'),
			'#prefix' => '<div id="variables_table">',
			'#suffix' => '</div>',
	  );

		$form['table_note'] = array(
      '#type' => 'fieldset',
      '#title' => t('Note'),
    );
    $form['table_note']['table_note'] = array(
      '#markup' => t('All variables that have been imported using Shape, CSV or PX are displayed in the table above. Variables are grouped by Variable name and Spatial layer, therefore each row presents a group of all variables with the same name that may have multiple dates defined.
											Actions Delete selected, Publish selected on date and Unpublish selected will affect all variables that correspond to the selected row.')
    );

		$form['#attached']['library'][] = 'stage2_admin/variables';

	  return $form;
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::validateForm().
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the form values.
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do something useful.
	$values = $form_state->getValues();

	$bla = $form_state->getTriggeringElement();
	$id = $bla["#parents"][0];

	if($id == "publish"){

		$parameters_to_pass = array();
		$parameters_to_pass['selected'] = array_filter(array_values($values['table']));
		$parameters_to_pass['attribute'] = 'var_names_id';

		$url = \Drupal\Core\Url::fromRoute('stage2_admin.variablesPublish')
				->setRouteParameters(array('id'=>json_encode($parameters_to_pass)));
		$form_state->setRedirectUrl($url);
	}
	elseif($id =='unpublish'){
		$variables = array_filter(array_values($values['table']));
		StageDatabaseSM::unpublishVariablesvar_names_id($variables);
	}
	elseif($id == "delete"){
		$variables = array_filter(array_values($values['table']));
		$bla = $form_state->getValues();
		$url = \Drupal\Core\Url::fromRoute('stage2_admin.StageVariableDeleteForm')
				  ->setRouteParameters(array('id'=>json_encode($variables)));
	$form_state->setRedirectUrl($url);
	}

  }

  function ajax_spatial_layer_callback($form, $form_state) {
	  return $form['table'];
	}
}
