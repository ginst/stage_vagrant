<?php
namespace Drupal\stage2_admin\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\stage2_admin\StageDatabase;
use Drupal\stage2_admin\StageDatabaseSM;
use Drupal\stage2_admin\StageFormsCommon;
use Drupal\Core\Database\Database;

class StageFlushConfirmForm extends ConfirmFormBase{

	/**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'stage_variable_delete_form';
  }

  public function getQuestion() {
    return t('Do you really want to flush test environemnt?');
  }

  public function getCancelUrl() {
		  $url = Url::fromUri('internal:/clientset');
      return $url;
  }

  public function getDescription() {
    return t('This option cannot be undone!');
  }

  public function getConfirmText() {
    return t('OK');
  }

  public function getCancelText() {
    return t('Cancel');
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $instanceName=StageFormsCommon::getInstanceName();
    if ($instanceName=='stage2_test'){
      drupal_set_message(t('You are already in the test instance!'),'error');
    }
    else if (StageFormsCommon::isTestStillCloning()){
      drupal_set_message(t('The cloning process is still in progress'),'error');
    }
    else{
      $conn=Database::getConnectionInfo('default');
      $pwd=$conn['default']['password'];
      $tmpfname = tempnam("/tmp", $instanceName);
      
      StageFormsCommon::markTestForCloning();
      
      $path=__DIR__."/../../install_scripts";
      exec("$path/flush.sh $pwd $tmpfname $path > /dev/null 2>/dev/null &");
    }
    
    $url = Url::fromUri('internal:/clientset');
    $form_state->setRedirectUrl($url);
  }
}
