<?php
namespace Drupal\stage2_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\stage2_admin\StageDatabase;

class StageCoordinateSpatialUnitsForm extends FormBase{

	/**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'stage_coordinate_spatial_units_form';
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

	  $users = StageDatabase::loadCoordinateSpatialUnits();

	  $header = array(
		'first_name' => t('Name'),
		'prefered' => t('Prefered'),
		'weight' => t('Weight'),
	  );

	  $options = array();

	  foreach ($users as $user) {
		$options[$user->id] = array(
      'first_name' => Link::fromTextAndUrl($user->name, Url::fromUri('internal:/stage_settings/spatial_units/edit/'.$user->id)),
			'prefered' => ($user->note_id && $user->note_id=='1') ? $user->note_id : '',
		  'weight' => $user->weight,
		);
	  }

    $form['coordinate_spatial_units']['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add spatial unit'),
    );
    $form['coordinate_spatial_units']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete selected'),
    );
	  $form['coordinate_spatial_units']['table'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#js_select' => false,
		'#empty' => t('No spatial units found'),
	  );

	  $form['coordinate_spatial_units']['pager'] = array(
		  '#type' => 'pager',
		  '#weight' => 10,
		);
    $form['coordinate_spatial_units']['table_note'] = array(
      '#type' => 'fieldset',
      '#title' => t('Note'),
    );
    $form['coordinate_spatial_units']['table_note']['table_note'] = array(
      '#markup' => t('The table shows the hierarchically arranged list of codes of spatial units. Prefered column indicates the spatial unit, which is shown as the first, as long as there are values for the selected variable.')
    );


	  return $form;
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::validateForm().
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the form values.
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do something useful.
	$bla = $form_state->getTriggeringElement();
	$id = $bla["#parents"][0];

	if($id == "add"){

		$url = \Drupal\Core\Url::fromRoute('stage2_admin.coordinateSpatialUnitsEdit')
				->setRouteParameters(array('id'=>-1));
		$form_state->setRedirectUrl($url);

	}elseif($id == "delete"){

		$bla = $form_state->getValues();

		$idsToDelete = array();
		foreach($bla['table'] as $key => $value)
		{
			if($value != 0){
				array_push($idsToDelete, $key);
			}
		}

		$url = \Drupal\Core\Url::fromRoute('stage2_admin.coordinateSpatialUnitsDelete')
			  ->setRouteParameters(array('id'=>json_encode($idsToDelete)));

		$form_state->setRedirectUrl($url);
	}

	return;
  }
}
