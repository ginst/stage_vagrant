<?php
namespace Drupal\stage2_admin\Form;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\colortime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\stage2_admin\Form\StageVariablesEditRawForm;
use Drupal\stage2_admin\StageDatabase;
use Drupal\stage2_admin\StageDatabaseSM;
use Drupal\stage2_client\StageClientSM;

class StageVariablesEditRawForm{

	public static function getRawForm(&$form_state, $id = NULL, $varId=NULL) {
		// get data to edit
		$currentDataRaw = StageDatabase::getVariableProperties($entry = array("id"=>$id));
		// $current = json_decode($currentDataRaw[0]->data);
		$current = isset($currentDataRaw[0])?json_decode($currentDataRaw[0]->data):json_decode("[]");
		$class_breaks = StageDatabase::getAdvancedSettings(array("setting"=>"class_breaks"));
		$class_methods = StageDatabase::getAdvancedSettings(array("setting"=>"classification_methods"));

		$special_values = StageDatabase::getSpecialValues($id);

		$form['color_palette'] = array(
			'#type' => 'color_brewer_element',
			'#default_value' => isset($current->color_palette)?$current->color_palette:"Pastel2",
			'#title' => "Color palette",
		);

    $form['description'] = array(
      '#type'=> 'text_format',
      '#title' => t('Description (time and spatial unit dependent)'),
      '#default_value' => isset($current->description)?$current->description:'',
	    '#format'=> 'full_html',
    );

    $decimals_options = StageClientSM::stage2_client_get_advanced_settings('decimals_options');
    $form['decimals'] = array(
			'#type' => 'select',
			'#title' => t('Number of decimals'),
			'#options' => $decimals_options,
			'#default_value' => isset($current->decimals)?$current->decimals:0,
		 );

		 $form['classification'] = array(
		  '#type' => 'radios',
		  '#title' => t('Classification'),
		  '#options' => array(0 => t('Auto classification'), 1 => t('Manual classification')),
		  '#attributes' => array(
				'class' => array('classification_input')
			),
			'#default_value' => isset($current->classification)?$current->classification:0,
		);

		$form['auto_classification'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('Classification'),
		  '#states' => array(
			'visible' => array(
				'.classification_input input' => array('value' => 0),
			)
			)
		);

		$cb_options = json_decode($class_breaks[0]->value,true);
		$form['auto_classification']['class_breaks'] = array(
			'#prefix' => '<div id="cb">',
			'#suffix' => '</div>',
			'#type' => 'select',
			'#title' => t('Number of class breaks'),
			'#description' => t('Excessive number of may corrupt the choropleth map display.'),
			'#options' => $cb_options,
			'#default_value' => isset($current->auto_classification->class_breaks)?$current->auto_classification->class_breaks:5,
		 );

		$form['auto_classification']['categorized']=[
			'#prefix' => '<div id="less_more">',
			'#suffix' => '</div>',
		];

		$form['auto_classification']['categorized']['or_less'] = array(
			'#type' => 'textfield',
			'#default_value' => isset($current->auto_classification->categorized->or_less),
			'#title' => t('Group values less or equal to the following value'),
			'#description' => t('this will be the first item in the legend'),
		 );

		 $form['auto_classification']['categorized']['or_more'] = array(
			'#type' => 'textfield',
			'#default_value' => isset($current->auto_classification->categorized->or_more),
			'#title' => t('Group values more or equal to the following value'),
			'#description' => t('this will be the last item in the legend'),
		 );

		 $ac_options = json_decode($class_methods[0]->value,true);
     unset($ac_options[0]);
		 $form['auto_classification']['interval'] = array(
			'#type' => 'select',
			'#title' => t('Method'),
      '#options' => $ac_options,
      '#default_value' => isset($current->auto_classification->interval)?$current->auto_classification->interval:4,
		 );

		 $form['manual_classification'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('Manual classification'),
		  '#states' => array(
			'visible' => array(
				'.classification_input input' => array('value' => 1),
			)
			)
		);

    if (isset($current->manual_classification->manual_breaks) && is_array($current->manual_classification->manual_breaks)) {
      $current->manual_classification->manual_breaks=implode(',',$current->manual_classification->manual_breaks);
    }

		$form['manual_classification']['manual_breaks'] = array(
			'#type' => 'textfield',
			'#default_value' => isset($current->manual_classification->manual_breaks)?$current->manual_classification->manual_breaks:"",
			'#states' => array(
				'required' => array(
					':input[name="parameters"]' => array('value' => 2),
				),
				'required' => array(
					':input[name="classification"]' => array('value' => 1),
				),
			)
		);

    $form['auto_classification']['legend']=array(
      '#markup'=> '<div id="auto-legend"></div>'
    );

    $form['manual_classification']['legend']=array(
      '#markup'=> '<div id="manual-legend"></div>'
    );

		/* SPECIAL VALUES */

		 $form['special_values'] = array(
		  '#type' => 'fieldset',
		  '#title' => t('Special values'),
			'#tree' => TRUE,
		);

		  $header = array(
			'value' => t('Special value'),
			'lc' => t('Legend caption'),
			'col' => t('Color'),
		  );


			$form['lc'] = array(
			'#type' => 'value',
		);
			$form['col'] = array(
			'#type' => 'value',
		);

		  $options = array();

			// save to form_state first time
			if(null === $form_state->getValue("pass_special_values")){
				// form special values list from database

				foreach ($special_values as $key => $value) {

					$arr = array(
					  'value' => $value->special_value,
					  'lc' => array('data'=>
							array(
								'#type' => 'textfield',
								'#title' => t('Legend caption'),
								'#title_display' => 'invisible',
								'#name' => 'lc['.$key.']',
								'#size' => 30,
								'#maxlength' => 30,
								'#value' => $value->legend_caption,
							),
						),
						'col' => array('data'=>
							array(
								'#type' => 'color',
								'#title' => t('Legend caption'),
								'#title_display' => 'invisible',
								'#name' => 'col['.$key.']',
								'#size' => 5,
								'#maxLengh' => 5,
								'#value' => $value->color,
							),
						),
					);
					$options[$key] = $arr;
				  }
			}else{
				$options = $form_state->getValue("pass_special_values");
			}

			$form['pass_special_values']= array(
				'#type' => 'hidden',
				'#value' => json_encode($options),
			);

		  $form['special_values']['special_values_table'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $options,
			'#empty' => t('No special values found'),
			'#prefix' => '<div id="ajax_remove">',
			'#suffix' => '</div>',
		  );

			$form['special_values']['delete_special_value_button'] = array(
				'#type' => 'submit',
				'#value' => t('Remove special value'),
				'#submit' => array('Drupal\stage2_admin\Form\StageVariablesEditRawForm::removeSpecialValue'),
				//'#limit_validation_errors' => array(),
				'#ajax' => array(
					// Function to call when event on form element triggered.
					//'callback' => array('Drupal\stage2_admin\Form\StageVariablesEditRawForm::refreshAjax'),
					'callback' => array('Drupal\Stage2_admin\Form\StageVariablesEditRawForm', 'refreshAjax'),
					// Javascript event to trigger Ajax. Currently for: 'onchange'.
					'event' => 'click',
					'wrapper' => 'ajax_remove',
				),
		  );



		/* SECIAL VALUES ADD NEW */

		$form['special_values']['add_special_value'] = array(
		  '#type' => 'details',
		  '#title' => t('Add special value'),
			'#open' => FALSE
		);

		$form['special_values']['add_special_value']['special_value'] = array(
			'#type' => 'textfield',
			  '#title' => t('Special value'),
			  '#size' => 30,
			  '#maxlength' => 30,
			  '#description' => t('Max length: 30'),
			);

		$form['special_values']['add_special_value']['legend_text'] = array(
			'#type' => 'textfield',
			  '#title' => t('Legend text'),
			  '#size' => 30,
			  '#maxlength' => 30,
			);

		$form['special_values']['add_special_value']['color'] = array(
			'#type' => 'color',
			  '#title' => t('Color'),
			  '#default_value' => '#ffffff',
			);

		$form['special_values']['add_special_value']['add_special_button'] = array(
				'#type' => 'submit',
				'#value' => t('Add'),
				//'#limit_validation_errors' => array(),
				'#submit' => array('Drupal\stage2_admin\Form\StageVariablesEditRawForm::addSpecialValue'),
				'#ajax' => array(
					// Function to call when event on form element triggered.
					'callback' => array('Drupal\stage2_admin\Form\StageVariablesEditRawForm', 'addSpecialValueCallback'),
					// Javascript event to trigger Ajax. Currently for: 'onchange'.
					'event' => 'click',
					'wrapper' => 'ajax_remove',
				),
		  );

		$gn=json_decode(db_query("SELECT value from s2.advanced_settings where setting='geonetwork'")->fetchField());
 		 // if ($gn->enable=='true'){
     //
			//  if ($gn->proxy){
			// 	 $form['inspire_xml_link'] = array(
			// 		 '#markup' => '<a href="'.$gn->proxy.'/geonetwork/srv/eng/catalog.search#/metadata/'.$varId.'">Geonetwork link</a>',
			// 	 );
			//  }
			//  $form['inspire_xml'] = array(
			// 	 '#type' => 'textarea',
			// 	 '#disabled' => TRUE,
			// 	 '#title' => t('INSPIRE link'),
			// 	 '#default_value' => StageDatabaseSM::stage_inspire('get',$varId),
			//  );
 		 // }

    if (!is_null($varId))
      $form['#attached']['drupalSettings']['variableValues']=StageClientSM::stage2_get_varval($varId,false,'en',false);

		return $form;
  }

  /**
	 * addSpecialValue.
	 */
	public static function addSpecialValue(array &$form, FormStateInterface $form_state) {
		$formValues = $form_state->getValues();

		$parent_array = getArray($form, 'special_values');

		// pridobi nove attribute
		$newRow = array(
					'value' => $parent_array['add_special_value']['special_value']['#value'],
					'lc' => array('data'=>
						array(
							'#type' => 'textfield',
							'#title' => t('Legend caption'),
							'#title_display' => 'invisible',
							'#name' => 'lc['.$key.']',
							'#size' => 30,
							'#maxlength' => 30,
							'#value' => $parent_array['add_special_value']['legend_text']['#value'],
						),
					),
					'col' => array('data'=>
						array(
									'#type' => 'color',
									'#title' => t('color'),
									'#title_display' => 'invisible',
									'#name' => 'col['.$key.']',
									'#size' => 10,
									'#maxLengh' => 10,
									'#value' => $parent_array['add_special_value']['color']['#value'],
						),
					)
		);

		$opt_array = getArray($formValues, "pass_special_values");
		$options = json_decode($opt_array, true);

		$equalFlag = false;

		$inputs = $form_state->getUserInput();
		$lcs=$inputs['lc'];
		$cols=$inputs['col'];

		foreach($options as $i=>&$option){
			if($option['value'] == $newRow['value']){
				$equalFlag = true;
			}
			$option['lc']['data']['#value']=$lcs[$i];
			$option['col']['data']['#value']=$cols[$i];
		}

		if(!$equalFlag){
			array_push($options, $newRow);
		}else{
			drupal_set_message(t('Special value already exists!'), 'error');
		}

		$form_state->setValue("pass_special_values", $options);
		$form_state->setRebuild();
	}

	 /**
	 * addSpecialValueAjaxCallback.
	 */
	public static function addSpecialValueCallback(array $form, FormStateInterface $form_state) {
		$parent_array = getArray($form, 'special_values');
		return $parent_array['special_values_table'];
	}

  /**
	 * removeSpecialValue.
	 *
	 * Submit handler which removes selected special values from tableselect element
	 */
	public static function removeSpecialValue(array &$form, FormStateInterface &$form_state) {

		$formValues = $form_state->getValues();

		$opt_array = getArray($formValues, "pass_special_values");
		//$results = getArray($formValues, "special_values_table"); // doesn't work for the first table item (always 0 => 0)
		$results=getArray($form, 'special_values_table');
		if (!isset($results['#value'])) return;

		$options = json_decode($opt_array, true);

		foreach($results['#value'] as $i => $value){
			unset($options[$i]);
		}

		$inputs = $form_state->getUserInput();
		$lcs=$inputs['lc'];
		$cols=$inputs['col'];

		foreach($options as $i=>&$option){
			$option['lc']['data']['#value']=$lcs[$i];
			$option['col']['data']['#value']=$cols[$i];
		}

		$form_state->setValue("pass_special_values", $options);
		$form_state->setRebuild();
	}

    /**
	 * Refreshes ajax.
	 */
	public static function refreshAjax(array &$form, FormStateInterface $form_state) {
		$sp_array = getArray($form, 'special_values');
		return $sp_array['special_values_table'];
	}

	/*
	* Returns JSON for builded array
	*/

	public static function saveParameters(array &$form, FormStateInterface &$form_state) {

    // dsm($form_state->getUserInput());
		// all manual_parameters data
		$formValues = $form_state->getValues();

		$manual_parameters_data = getArray($formValues, 'manual_parameters');
		unset($manual_parameters_data['special_values']);

		// get table special values from form
		$array = getArray($form, 'special_values_table');

		// add special values to results
		$manual_parameters_data['special_values'] = $array['#options'];

		unset($manual_parameters_data['pass_special_values']);
		unset($manual_parameters_data['special_values']);

		unset($manual_parameters_data['lc']);
		unset($manual_parameters_data['col']);

		$manual_parameters_data['description'] = $manual_parameters_data['description']['value'];
		$id = StageDatabase::updateVariableProperties(json_encode($manual_parameters_data));

		$inputs = $form_state->getUserInput();

		$values = $form_state->getValues();


		// TODO: save special values to new database
		StageDatabase::insertSpecialValues($id, $array['#options'],$inputs);

		return $id;
	}
}

	/*
	* Function finds subarray of array specified by index as associative key
	*
	* @$array	array	Main array with subarray included
	* @$index	string	Associative key of array which defines subarray
	*/
		function getArray($array, $index) {
			if (!is_array($array)) return null;
			if (isset($array[$index])) return $array[$index];
			foreach ($array as $item) {
				$return = getArray($item, $index);
				if (!is_null($return)) {
					return $return;
				}
			}
			return null;
		}
