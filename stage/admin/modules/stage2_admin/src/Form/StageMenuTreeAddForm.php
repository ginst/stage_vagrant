<?php
namespace Drupal\stage2_admin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\stage2_admin\StageDatabase;
use Drupal\stage2_admin\StageDatabaseSM;
use Drupal\stage2_admin\StageFormsCommon;
use Drupal\stage2_admin\Form\StageVariablesEditRawForm;

class StageMenuTreeAddForm extends FormBase{

	private $id;
	private $varIds;

	/**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'stage_menu_tree_form';
  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   *
   *	@id		integer		Tree node id
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;

	// current variable data
	$entry = array("var_tree_id" => $id);
	$default = StageDatabase::getVariableName($entry);

	// List of all existing all_acronyms
	$all_acronyms = StageDatabaseSM::load_all_acronyms();
	// list of all variables with saved values
	$allVariables = StageDatabase::loadVariables();
	// get all tree subelements of tree element
	$childrens = StageDatabase::getAllTreeChildrens($id);
	// get all variable names of specified tree elements (if none, hide variable properties)
	$variableNames = StageDatabase::loadMenuTree($childrens);
	// get tree structure
	$tree_menu=StageFormsCommon::treeStructure();

	// save variables to array
	$namesIds = array();
	foreach($variableNames as $names){
		array_push($namesIds, $names->id);
	}

	// get all existing variable values of selected variables
	$variables = StageDatabase::loadVariablesNoNames(array("var_names_id" => $namesIds));

	// save variable values ids to array
	$varIds = array();
	foreach($variables as $var){
		array_push($varIds, $var->id);
	}
	$this->varIds = $varIds;

	$form['mentu_tree_edit'] = array(
		'#type' => 'details',
		'#title' => t('Menu tree editor'),
		'#open' => true,
	);

	// prepare containers
	$form['data_container'] = array(
		'#type' => 'data_container',
		'#prefix' => '<div id="data_container">',
		'#suffix' => '</div>',
	);
	$form['mentu_tree_edit']['left_container'] = array(
		'#type' => 'container',
		'#attributes' => array('class' => array('element-column-left'))
	);
	$form['mentu_tree_edit']['right_container'] = array(
		'#type' => 'container',
		'#attributes' => array('class' => array('element-column-right'))
	);
	$form['mentu_tree_edit']['calear_container'] = array(
			'#type'=> 'container',
			'#attributes' => array('class' => array('element-clear-fix'))
	);

	$form['mentu_tree_edit']['left_container']['path'] = array(
		'#type' => 'textfield',
		'#title' => $this->t('Tree path'),
		'#size' => 60,
		'#maxlength' => 128,
		'#description' => t('Variable name path in tree structure.'),
		'#value' => $tree_menu[$this->id]['path'],
		'#disabled' => TRUE,
	);

	$form['mentu_tree_edit']['left_container']['name'] = array(
		'#type' => 'textfield',
		'#title' => $this->t('Name'),
		// '#size' => 60,
		// '#maxlength' => 300,
		'#required' => TRUE,
		'#description' => t('Variable name - English name is to be displayed in the client menu.'),
		'#default_value' => $default[0]->name,
	);

	$form['mentu_tree_edit']['left_container']['acronym_all'] = array(
		'#type' => 'textfield',
		'#name' => 'existing_acronyms',
		'#default_value' => $all_acronyms,
	);

	$form['mentu_tree_edit']['left_container']['acronym'] = array(
		'#type' => 'textfield',
	  '#title' => $this->t('Acronym'),
	  '#size' => 60,
	  '#maxlength' => 10,
		'#required' => TRUE,
		'#default_value' => $default[0]->short_name,
		'#description' => t('Variable acronym to be used when user imports data. The input will be automatically converted to uppercase.'),
		);
		$popup_title = $default[0]->popup_title;
		if ($popup_title ==''){
			$popup_title = $tree_menu[$this->id]['path'];
		}
	$form['mentu_tree_edit']['left_container']['popup_title'] = array(
		'#type' => 'textfield',
	  '#title' => $this->t('Popup title'),
		'#default_value' => $popup_title,
		'#description' => t('The name that is to be displayed when user clicks on a poligon.'),
		);

	$form['mentu_tree_edit']['left_container']['legend_title'] = array(
		'#type' => 'textfield',
	  '#title' => $this->t('Legend caption'),
		'#default_value' => $default[0]->legend_title,
		'#description' => t('The legend caption.'),
		);
	// Variable description that is not dependent of date and spatial unit
  // check if description is set for indivisual spatial units if so disable editing. Editing can be reenabled by following checkbox
  // $form['mentu_tree_edit']['right_container']['warning'] = array(
	// 	'#markup' => '<div name = "desc_warning" class="messages messages--warning">Description changes may have effect on subordinated entries.</div>',
  // );
  $form['mentu_tree_edit']['right_container']['description'] = array(
		'#type' => 'text_format',
	  '#default_value' => $default[0]->description,
		// '#name' => 'variable_description_name',
		'#title' => t('Variable description that is not dependent of date and spatial unit.'),
		// '#attributes' => array('id' => 'textarea-description'),
    '#format'=> 'full_html',
		);

	// The following two fields will be hidden using JS. They are used as reference in the submitt function.
	$form['mentu_tree_edit']['left_container']['variable_id'] = array(
		 '#type' => 'textfield',
		 '#size' => 10,
		 '#name' =>'variable_id_field',
		 '#disabled' => TRUE,
		 '#value' => $id,
		 );
	$form['mentu_tree_edit']['left_container']['dependent_variables'] = array(
		 '#type' => 'textfield',
		 '#name' =>'dependent_variables_field',
		 '#size' => 128,
		 '#disabled' => TRUE,
		 '#value' => $varIds,
		 );

	// prepare containers
	$form['mentu_tree_edit']['picture'] = array(
		'#type' => 'details',
		'#title' => t('Picture'),
		'#open' => false,
	);
	$form['mentu_tree_edit']['picture']['left_container'] = array(
		'#type' => 'container',
		'#attributes' => array('class' => array('element-column-left'))
	);
	$form['mentu_tree_edit']['picture']['right_container'] = array(
		'#type' => 'container',
		'#id' => 'picture_preview_container',
		'#attributes' => array('class' => array('element-column-right'))
	);
	$form['mentu_tree_edit']['picture']['calear_container'] = array(
			'#type'=> 'container',
			'#attributes' => array('class' => array('element-clear-fix'))
	);

	$form['mentu_tree_edit']['picture']['left_container']['picture_upload_recommendations'] = array(
		'#markup'=>t('Choose the picture that is to be displayed in client variable header section.</br> Recomended picture size 370 x 130 px.'),
	);
	$form['mentu_tree_edit']['picture']['left_container']['upload'] = array(
		'#type' => 'managed_file',
		'#multiple'=> FALSE,
		'#title' => t('Choose a picture'),
		'#description' => t('Allowed extensions: png., Max. size: 0.5 MB '),
		'#upload_location' => 'public://temp_png_uploads',
		'#upload_validators' => [
			'file_validate_is_image'      => array(),
			'file_validate_extensions'    => array('png'),
			'file_validate_size'          => array(500000)
		]
	);

	$pic = StageDatabaseSM::fetchVarPic($id); //Fetch pic from the s2.var_names:picture

	$form['mentu_tree_edit']['picture']['right_container']['picture_preview'] = array(
  	'#type' => 'textarea',
  	'#name' => 'picture_textarea',
		'#value'=> $pic,
	);


	$form['mentu_tree_edit']['picture']['right_container']['remove_picture'] = array(
	'#type' => 'button',
	'#name' => 'remove_picture',
	'#value' => t('Remove picture'),
	'#attributes' => array('onclick' => 'return (false);'),
	);


	// Prepare containers
	$form['mentu_tree_edit']['delineation'] = array(
		'#type' => 'details',
		'#title' => t('Delineation'),
		'#open' => false,
	);
	$form['mentu_tree_edit']['delineation']['left_container'] = array(
		'#type' => 'container',
		'#attributes' => array('class' => array('element-column-left'))
	);
	$form['mentu_tree_edit']['delineation']['right_container'] = array(
		'#type' => 'container',
		'#attributes' => array('class' => array('element-column-right')),
		'#prefix' => '<div id="delineation_container_ajax">',
		'#suffix' => '</div>',
	);
	$form['mentu_tree_edit']['delineation']['calear_container'] = array(
			'#type'=> 'container',
			'#attributes' => array('class' => array('element-clear-fix')));

	$available_acronyms = StageDatabaseSM::stage2GetAvailableAcronyms();
	$delineation_formula = StageDatabaseSM::stage2getdelineationformula($id);
	$tree_menu=StageFormsCommon::treeStructure();
	$opt = [];
	foreach ($available_acronyms as $key => $value) {
		$opt[$value] = $tree_menu[$key]['path'].' {'.$value.'}';
	}

		$form['mentu_tree_edit']['delineation']['del_var_name'] = array(
			'#type' => 'select',
      '#empty_value' => '--',
			'#options' => $opt,
		);

		$form['mentu_tree_edit']['delineation']['add_var_submit'] = array(
		'#type' => 'button',
		'#name' => 'update_delineation_formula',
		'#value' => t('Add variable'),
		'#attributes' => array('onclick' => 'return (false);'),
		);

		$form['mentu_tree_edit']['delineation']['add_var_area'] = array(
		'#type' => 'button',
		'#value' => t('Add area'),
		'#name' => 'add_area',
		'#attributes' => array('onclick' => 'return (false);'),
		);


		$form['mentu_tree_edit']['delineation']['left_container']['formula'] = array(
			'#type' => 'textarea',
			'#name' => 'delineation_formula',
			'#title' => $this->t('Delineation formula. Alowed input / * - + and variable acronyms in curly braces'),
			'#value' => $delineation_formula,
			'#description' => t('Please test the validity of the formula before publishing the variable.')
		);
		$form['mentu_tree_edit']['delineation']['right_container']['del_allow'] = array(
			'#type' => 'checkbox',
	  	'#name' => 'delineation_disabled',
			'#title' => $this->t('Delineation disabled'),
			'#description' => t('If checked delineation chart will not be rendered.')
		);


		//prepare containers - parametrs
		$form['mentu_tree_edit']['parameters_details'] = array(
			'#type' => 'details',
			'#title' => t('Parameters'),
			'#open' => false,
		);
		$form['mentu_tree_edit']['parameters_details']['parameters'] = array(
			'#type' => 'radios',
			'#default_value' => 0,
			'#options' => array(0 => $this->t('No change'),
													1 => $this->t('Load existing parameters'),
													2 => $this->t('Manual parameters'),
													3 => $this->t('Default parameters')),
		);

	// Disable parametrs section if no data is available for the selected variable.
	if(empty($variables)){
		$form['mentu_tree_edit']['parameters_details']['parameters']['#disabled'] = true;
		$form['mentu_tree_edit']['parameters_details']['warning'] = array(
			'#markup' => t('Parameters can be changed if at least one variable is assigned to given menu tree entry. Of if the selected menu entry has subordinated nodes.')
		);
	}
	else{

	// //** Change parametrs of the selected menu entry based on the existing parametrs TODO: add option to select spatial unit.
	$form['mentu_tree_edit']['existing_parameters'] = array(
	  '#type' => 'fieldset',
	  '#title' => $this->t('Load existing parameters'),
	  '#states' => array(
		'visible' => array(
			':input[name="parameters"]' => array('value' => 1),
		)
		),
		'#tree' => TRUE,
	);

	$form['mentu_tree_edit']['existing_parameters']['exist_var_name'] = array(
		'#title' => $this->t('Select variable'),
		'#type' => 'select',
		'#default_value' => $default[0]->short_name,
		'#options' => $opt,
	);
	// dsm($opt);
	$exist_var_su_opt =[];
	$exist_var_su_def =0;
	foreach (StageDatabase::loadCoordinateSpatialUnits() as $key => $value) {
		$exist_var_su_opt[$value->id] = $value->name;
		if ($value->note_id == '1'){
			$exist_var_su_def = $value->id;
		}
	}
	$form['mentu_tree_edit']['existing_parameters']['exist_var_su'] = array(
		'#title' => $this->t('Spatial unit'),
		'#type' => 'select',
		'#options' => $exist_var_su_opt,
		'#default_value' => $exist_var_su_def,
	);

	// Set date
	 $form['mentu_tree_edit']['existing_parameters']['exist_var_date'] = array(
		'#type' => 'date',
		'#title' => $this->t('Date'),
		'#attributes' => array('type'=> 'date', 'min'=> '-25 years', 'max' => '+5 years' ),
		'#default_value' => date("Y-m-d",strtotime('today')),
	 );

	//Set manual parametrs
	// get current default id properties
	$did = StageDatabase::getDefaultVariableProperties();
	$defid = isset($did[0])?$did[0]->id:NULL;
	// Load default parameters to start with
	$form['mentu_tree_edit']['manual_parameters'] = StageVariablesEditRawForm::getRawForm($form_state,$defid);
	$form['mentu_tree_edit']['manual_parameters']['#type'] = 'fieldset';
	$form['mentu_tree_edit']['manual_parameters']['#title'] = $this->t('Set manual parameters');
	$form['mentu_tree_edit']['manual_parameters']['#states'] = array(
		'visible' => array(
			':input[name="parameters"]' => array('value' => 2),
		)
	);
	$form['mentu_tree_edit']['manual_parameters']['#tree'] = TRUE;
	}

	// Submit button
  $form['mentu_tree_edit']['submit'] = array
	  (
		'#type' => 'submit',
		'#name' => 'save_btn',
		'#value' => t('Save'),
	  );
	// Submit button
	$form['mentu_tree_edit']['cancel'] = array(
		'#type' => 'link',
		'#title' => 'Cancel',
		'#attributes' => array(
			'class' => array('button'),
		),
		'#url' => Url::fromRoute('stage2_admin.menuTree'),
	);

	$form['#attached']['library'][] = 'stage2_admin/StageMenuTreeAddForm';
  return $form;
}

	//** The section below is related to the StageVariablesEditRawForm
	//           it is a result of the Drupal rigidity
	//addSpecialValue.
	public function addSpecialValue(array &$form, FormStateInterface $form_state) {
		//add special value in database and refresh table
		return $form['special_values_table'];
	}
	//Refreshes ajax.
	public function refreshAjax(array &$form, FormStateInterface $form_state) {
		return $form['special_values_table'];
	}


	//Implements \Drupal\Core\Form\FormInterface::validateForm().
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the form values.

  }
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

	$report = '';

	// save to variables
	$entry = array(
		  'text' => $form_state->getValue('name'),
		  'acronym' => strtoupper($form_state->getValue('acronym')),
      'description' => $form_state->getValue('description')['value'],
      'popup_title' => $form_state->getValue('popup_title'),
			'legend_title' => $form_state->getValue('legend_title'),
		);

	$variable_id = $form['mentu_tree_edit']['left_container']['variable_id']['#value'];
	$entry['id'] = $variable_id;
	$count = StageDatabase::renameMenuTree($entry); // Modify name text and description

	$report += $count.' of variables modified in the form menu tree editor.';


	// save/update delineation formula
	$values = $form_state->getUserInput();

	if (isset ($values['delineation_formula'])){

		$formula = $values['delineation_formula'];
	}
	else{
		$formula = 'DELINEATION_DISABLED';
	}
	StageDatabaseSM::stage2updatedelineationformula($formula,$variable_id);


	// save the picture
	$image = $form_state->getValue('upload');
	if ($image){
		$file = file_load($image[0]);
		$uri = $file->uri;
		$full_filename = $uri->value;
		$path = $full_filename;
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		$encoded_img = '<img alt="Embedded Image" src="'.$base64.'" />';
		StageDatabaseSM::stage2picture2blob($encoded_img,$variable_id);
		$report += '; image update';
	}
	$image = $form_state->getValue('upload');


	if(isset($values['picture_textarea']) && $values['picture_textarea']=='picture_removed'){
			StageDatabaseSM::stage2picture2blob(false,$variable_id);
	}

	// save to variable parameters
	// if is set manual parameters, save JSON to var_properties
	$choose = $form_state->getValue('parameters');

	if(!empty($choose)){
		if($choose == 0){
			// get default property id
			$propId = StageDatabase::getDefaultVariableProperties();

			// update parameters in var_values
			StageDatabase::updateVariableValues(array("var_properties_id"=>$propId[0]->id), $form_state->getValue('dependent_variables'));
		}
		elseif($choose == 1)	// existing parameters
		{


			$exist_var_name = $form_state->getValue(['existing_parameters', 'exist_var_name']);
			$exist_var_su= $form_state->getValue(['existing_parameters', 'exist_var_su']);
			$exist_var_date = $form_state->getValue(['existing_parameters', 'exist_var_date']);

			// get closest variable value parameters id
			$prid = StageDatabaseSM::stage_get_existing_param($exist_var_name,$exist_var_su,$exist_var_date);
			if(!$prid){
				drupal_set_message('The parameters were not available for the combination variable name - spatial unit. ','warning');
				return;
			}
			$prid = $prid ? $prid : StageDatabase::getDefaultVariableProperties()[0]->id;

			// update parameters in var_values
			StageDatabase::updateVariableValues(array("var_properties_id"=>$prid), $form_state->getValue('dependent_variables'));
		}
		elseif($choose == 2)	// manual parameters
		{
			// save parameters to database and get its id
			$propId = \Drupal\stage2_admin\Form\StageVariablesEditRawForm::saveParameters($form, $form_state);
			// update parameters in var_values
			StageDatabase::updateVariableValues(array("var_properties_id"=>$propId), $form_state->getValue('dependent_variables'));
		}
		$varCount = count($form_state->getValue('dependent_variables'));
		drupal_set_message(t($varCount.' variable values updated with selected properties.'), 'status');
	}

	// prepare json and save to var_properties
	$single = $form_state->getValue(['manual_parameters', 'color_pallete']);

	$values = $form_state->getValues();

	StageDatabaseSM::stageLog('menu tree','Tree item settings changed for id: '.$variable_id);


	// redirect back to menu_tree
		$url = Url::fromUri('internal:/menu_tree');
		$form_state->setRedirectUrl($url);
  }

	//*******Ajax return functions *************
	function stage_ii_delineation_container_ajax_callback(array &$form, FormStateInterface $form_state){
		return $form['mentu_tree_edit']['delineation']['right_container'];
	}
	// custom submit FUNCTIONS
	function update_delineation_formula(array &$form, FormStateInterface $form_state){

		$trugger = $form_state->getTriggeringElement()["#parents"][0];
		$values = $form_state->getUserInput();
		$variable_id = $form['mentu_tree_edit']['left_container']['variable_id']['#value'];
		$formula = $values['formula'];

		switch($trugger){
			case 'add_var_area':
				$formula = $formula.'{area}';
				break;
			case 'add_var_submit':
				$selected_acronym_id = $values['del_var_name'];
				$selectedacronymtext =  $form['mentu_tree_edit']['delineation']['left_container']['del_var_name']['#options'][$selected_acronym_id];
				$formula = $formula.'{'.$selectedacronymtext.'}';
				break;

		}
		StageDatabaseSM::stage2updatedelineationformula($formula,$variable_id);
		$form_state->setRebuild(true);
	}
}
