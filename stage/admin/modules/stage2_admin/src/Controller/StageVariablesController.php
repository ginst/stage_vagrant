<?php

namespace Drupal\stage2_admin\Controller;

use Drupal\stage2_admin\StageSettings\database;
use Drupal\Core\Form\FormInterface;

class StageVariablesController{
	
	function init(){
		
		$form['variables'] = array(
		  '#type' => 'details',
		  '#title' => t('Variables'),
		  '#open' => TRUE,
		);
		
		$form['variables']['form'] = \Drupal::formBuilder()->getForm('Drupal\stage2_admin\Form\StageVariablesForm');
			
		return $form;
	}
}