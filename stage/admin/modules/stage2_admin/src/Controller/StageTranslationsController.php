<?php

namespace Drupal\stage2_admin\Controller;

use Drupal\stage2_admin\StageSettings\database;
use Drupal\Core\Form\FormInterface;

class StageTranslationsController{

	function init(){

		$form['StageTranslationsController'] = array(
			'#type' => 'details',
			'#title' => t('Translations'),
			'#open' => true,
			'#prefix' => '<div id="languages_container">',
			'#suffix' => '</div>',
		);

		$form['StageTranslationsController']['form'] = \Drupal::formBuilder()->getForm('Drupal\stage2_admin\Form\StageTranslationsForm',false);
    //
    //
		// $form['form'] = \Drupal::formBuilder()->getForm('Drupal\stage2_admin\Form\StageAdvancedSettingsForm',false);



		return $form;

	}
}
